# Choose and name our temporary image.
FROM alpine as intermediate
# Add metadata identifying these images as our build containers
LABEL stage=intermediate

ADD https://nexus.fernando.engineer/repository/fjcapeletto-releases/engineer/fernando/CMSAVRO/1.1/CMSAVRO-1.1.zip .
RUN unzip CMSAVRO-1.1.zip

FROM python:3.8-slim-buster
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
COPY --from=intermediate CMSAVRO-1.1 .
CMD ["python3", "-m", "flask", "run", "--host=0.0.0.0"]