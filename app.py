import os
from pickle import FALSE, TRUE
from xmlrpc.client import boolean
import requests
import json
import avro
from avro.io import DatumWriter, DatumReader
from avro.datafile import DataFileReader, DataFileWriter
#import avro_json_serializer
import io
from flask import Flask, render_template, request, redirect
from flask_wtf.csrf import CSRFProtect
from flask import session, url_for
from logging.config import dictConfig

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})


app = Flask(__name__)
app.secret_key = 'For now just making SonarQube happy'
CSRFProtect(app)
mainpage = os.getenv('CMS_URL', 'http://cms.lab.fernando.engineer/')
app_handler_component = os.getenv('CMS_HANDLER_COMPONENT', 'trackhandler')
app_actordb_component = os.getenv('CMS_ACTORDB_COMPONENT', 'trackactordb')
app_handler_component_port = os.getenv('CMS_HANDLER_COMPONENT_PORT', '1977')
app_actordb_component_port = os.getenv('CMS_ACTORDB_COMPONENT_PORT', '1981')
app_handler_url = 'http://'+app_handler_component+':'+app_handler_component_port;
app.logger.info('app_handler_url: %s', app_handler_url)
app_actordb_url = 'http://'+app_actordb_component+':'+app_actordb_component_port;
app.logger.info('app_actordb_url: %s', app_actordb_url)
aivdmhandler_aisdecoder = app_handler_url+'/aisdecoder/'
aivdmhandler_aistracks = app_handler_url+'/aistracks/'
aivdmhandler_ownships = app_handler_url+'/ownships/'
aivdmhandler_ownship = app_handler_url+'/ownship/'
aivdmhandler_ownship_history = aivdmhandler_ownship
aivdmhandler_ownship_detections = aivdmhandler_ownship
aivdmhandler_weapons = app_handler_url+'/weapons/'
aivdmhandler_weapon = app_handler_url+'/weapon/' 
aivdmhandler_weapon_detections = aivdmhandler_weapon
aivdmhandler_weapon_trail = aivdmhandler_weapon
aivdmhandler_weapon_delete = aivdmhandler_weapon
aivdmhandler_fire = app_handler_url+'/fire/'
actorsdb_url = app_actordb_url+'/actor/'

@app.route('/', methods=['GET', 'POST'])
def init():
    headers = {'Content-Type': 'application/json',}
    form = request.form 
    if request.method == 'POST':
        returning_lat = request.form['ownship_lat']
        returning_lng = request.form['ownship_lng'] 
        ownship_selected = request.form['ownship_selected'] 
        ownship_zoom = request.form['ownship_zoom']
        ownship_dict = {}
        ownship_dict['mmsi'] = request.form['ownship_mmsi']
        ownship_dict['latitude'] = request.form['ownship_lat']
        ownship_dict['longitude'] = request.form['ownship_lng']        
        ownship_dict['speedOverGround'] = request.form['ownship_sog']
        ownship_dict['courseOverGround'] = request.form['ownship_cog']   
        json_fullstr = json.dumps(ownship_dict)
        response = requests.post(aivdmhandler_ownship, headers=headers, data=json_fullstr)
        response_json = response.json()
        if 'error' in response_json :
            error_msg = response_json['error']
            return render_template('now.html', form=form, TrackHandler=error_msg)
        return render_template('now.html', form=form, ownship_zoom=ownship_zoom, ownship_selected=ownship_selected, returning_lat=returning_lat,returning_lng=returning_lng)  
    else :
        return render_template('now.html', form=form) 

@app.route('/inject', methods=['GET', 'POST'])
def inject():
    headers = {'Content-Type': 'application/json',}
    form = request.form
    if request.method == 'POST':
        textarea = request.form['aivdmstring']
        aivdm_msg_list = textarea.replace('\r','').split('\n')
        json_fullstr = json.dumps(aivdm_msg_list)
        app.logger.info('aivdmhandler_aisdecoder: %s', aivdmhandler_aisdecoder)
        try:
            response = requests.post(aivdmhandler_aisdecoder, headers=headers, data=json_fullstr)
            response_json = response.json()
            if 'error' in response_json :
                error_msg = response_json['error']
                return render_template('inject.html', form=form, TrackHandler=error_msg)
            return redirect(mainpage,code=303)
        except:
            app.logger.error('Impossible to connect to : %s', aivdmhandler_aisdecoder)
            return "error"
    else :
        return render_template('inject.html', form=form)

@app.route('/aistracks')
def aistracks():
    headers = {'Content-Type': 'application/json',}
    app.logger.info('Url to get AISTRACKS : %s', aivdmhandler_aistracks)
    try:
        response = requests.get(aivdmhandler_aistracks,headers=headers)
        response_json = response.json();
        return json.dumps(response_json)
    except: 
        app.logger.error('Impossible to connect to : %s', aivdmhandler_aistracks)
        return "error"

@app.route('/reset')
def reset():
    requests.delete(aivdmhandler_aistracks)
    return redirect(mainpage)

@app.route('/ownreset')
def reset_ownships():
    requests.delete(aivdmhandler_ownships)
    return redirect(mainpage)

@app.route('/<int:mmsi>/del')
def delete(mmsi):
    str_mmsi = str(mmsi)
    url = str(aivdmhandler_aistracks+str_mmsi)
    app.logger.info('Url to delete AISTRACK : %s', url)
    requests.delete(url)
    return str_mmsi + " deleted."

@app.route('/<int:mmsi>/deleteOwnShip')
def delete_ownship(mmsi):
    str_mmsi = str(mmsi)
    url = str(aivdmhandler_ownship+str_mmsi)
    app.logger.info('Url to delete Ownship : %s', url)
    requests.delete(url)
    return str_mmsi + " ownship deleted."

@app.route('/actors')
def actors():
    headers = {'Content-Type': 'application/json',}
    response = requests.get(actorsdb_url,headers=headers)
    response_json = response.json();
    return json.dumps(response_json)

@app.route('/ownships')
def ownships():
    headers = {'Content-Type': 'application/json',}
    response = requests.get(aivdmhandler_ownships,headers=headers)
    response_json = response.json();
    return json.dumps(response_json)

@app.route('/<int:mmsi>/history')
def ownship_history(mmsi):
    str_mmsi = str(mmsi)
    headers = {'Content-Type': 'application/json',}
    response = requests.get(aivdmhandler_ownship_history+str_mmsi,headers=headers)
    response_json = response.json();
    return json.dumps(response_json)


@app.route('/ownship', methods=['GET', 'POST'])
def ownship():
    #NEW CODE
    track_schema = avro.schema.parse(open("schemas/Track.avsc", "rb").read())
    app.logger.info('Loaded AVRO Schema: Track.avsc -> : %s', str(track_schema))
    #avro_schema = avro.schema.make_avsc_object(track_schema, avro.schema.Names())
    #serializer = avro_json_serializer.AvroJsonSerializer(avro_schema)
    buf = io.BytesIO()
    writer = DataFileWriter(buf, DatumWriter(), track_schema)
    # END NEW CODE
    # OLD CODE
    #headers = {'Content-Type': 'application/json',}
    # END OLD CODE
    form = request.form
    if request.method == 'POST':
        ownship_dict = {}
        #OLD CODE
        #ownship_dict['mmsi'] = request.form['ownship_mmsi']
        #ownship_dict['latitude'] = request.form['ownship_lat']
        #ownship_dict['longitude'] = request.form['ownship_lng']       
        #ownship_dict['speedOverGround'] = request.form['ownship_sog']
        #ownship_dict['courseOverGround'] = request.form['ownship_cog']  
        #json_ownship_dict = json.dumps(ownship_dict)
        #END OLD CODE
        # NEW CODE
        ownship_dict['trackId'] = int(request.form['ownship_mmsi'])       
        ownship_dict['time'] = 0
        ownship_dict['sensorType'] = "OWNSHIP"     
        ownship_dict['trackType'] = "UNKNOWN"    
        position_dict = {}
        position_dict['latitude'] = float(request.form['ownship_lat']) 
        position_dict['longitude'] = float(request.form['ownship_lng'])
        ownship_dict['position'] = position_dict       
        ownship_dict['speedOverGround'] = float(request.form['ownship_sog'])
        ownship_dict['courseOverGround'] = float(request.form['ownship_cog'])  
        ownship_dict['maneuvered'] = boolean(TRUE)
        ownship_dict['hit'] = boolean(FALSE)
        writer.append(ownship_dict)
        writer.flush()
        buf.seek(0)
        msg = buf.read()
        #json_str = serializer.to_json(ownship_dict)
        response = requests.post(aivdmhandler_ownship, data=msg)
        #END NEW CODE
        # OLD CODE
        #response = requests.post(aivdmhandler_ownship, headers=headers, data=json_ownship_dict)
        # END OLD CODE
        response_json = response.json()
        if 'error' in response_json :
            error_msg = response_json['error']
            return render_template('now.html', form=form, TrackHandler=error_msg)
        return render_template('now.html', form=form)  
    else :
        return render_template('now.html', form=form) 

@app.route('/<int:mmsi>/sog/<int:sog>')
def ownship_setspeed(mmsi,sog):
    str_mmsi = str(mmsi)
    str_sog = str(sog)
    headers = {'Content-Type': 'application/json',}
    response = requests.get(aivdmhandler_ownship_history+str_mmsi+'/sog/'+str_sog,headers=headers)
    response_json = response.json();
    return json.dumps(response_json)

@app.route('/<int:mmsi>/cog/<int:cog>')
def ownship_setcourse(mmsi,cog):
    str_mmsi = str(mmsi)
    str_cog = str(cog)
    headers = {'Content-Type': 'application/json',}
    response = requests.get(aivdmhandler_ownship_history+str_mmsi+'/cog/'+str_cog,headers=headers)
    response_json = response.json();
    return json.dumps(response_json)

@app.route('/<int:mmsi>/detections')
def ownship_detections(mmsi):
    str_mmsi = str(mmsi)
    headers = {'Content-Type': 'application/json',}
    response = requests.get(aivdmhandler_ownship_detections+str_mmsi+'/detections/',headers=headers)
    response_json = response.json();
    return json.dumps(response_json)

@app.route('/weapons')
def weapons():
    headers = {'Content-Type': 'application/json',}
    response = requests.get(aivdmhandler_weapons,headers=headers)
    response_json = response.json();
    return json.dumps(response_json)

@app.route('/<int:mmsi>/fire')
def ownship_fire(mmsi):
    str_mmsi = str(mmsi)
    headers = {'Content-Type': 'application/json',}
    response = requests.get(aivdmhandler_fire+str_mmsi,headers=headers)
    response_json = response.json();
    return json.dumps(response_json)

@app.route('/<string:wid>/weapontrail')
def weapon_history(wid):
    str_wid = str(wid);
    headers = {'Content-Type': 'application/json',}
    response = requests.get(aivdmhandler_weapon_trail+str_wid,headers=headers)
    response_json = response.json();
    return json.dumps(response_json)

@app.route('/<string:wid>/detections')
def weapon_detections(wid):
    str_wid = str(wid);
    headers = {'Content-Type': 'application/json',}
    response = requests.get(aivdmhandler_weapon_detections+str_wid+'/detections/',headers=headers)
    response_json = response.json();
    return json.dumps(response_json)
 
@app.route('/abortfires')
def reset_weapons():
    requests.delete(aivdmhandler_weapons)
    return redirect(mainpage)

@app.route('/<string:wid>/abortfire')
def delete_weapon(wid):
    str_wid = str(wid)
    url = str(aivdmhandler_weapon_delete+str_wid)
    app.logger.info('Abort Fire : %s', url)
    print(url)
    requests.delete(url)
    return str_wid + " weapon deleted."
